#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define BUFFERSIZE 1024

int main (int argc, char *argv[])
{
	printf("TCP Client program by Megana for Lab 2 – SecAD - Spring 2019\n");
	if(argc != 2)
	{
		printf("Usage: %s <URL>\n", argv[0]);
		exit(0);
	}
	char *URL= argv[1];
	char servername[300];
	char path[3000];
	if(strlen(URL) > 3000)
	{
		perror("servername or path is too long\n");
		exit(1);
	}
	char * urlPattern = "http://%[^/]/%s/%s";
	sscanf(URL, urlPattern, servername, path); //extracting serverbname & path from URL using urlPattern
	printf("servername: %s ,path: %s \n", servername,path); 
	int sockfd = socket(AF_INET, SOCK_STREAM,0);
	if(sockfd < 0)
	{
		perror("error is opening a socket!\n");
		exit(2);
	}
	printf("A socket is opened\n");
	struct addrinfo hints, *serveraddr;
	memset(&hints, 0, sizeof hints);
	hints.ai_family= AF_INET;
	hints.ai_socktype= SOCK_STREAM;
	int addr_lookup= getaddrinfo(servername , "http", &hints, &serveraddr);
	if (addr_lookup!= 0)
	{
		fprintf(stderr, "getaddrinfo: %s\n",gai_strerror(addr_lookup));
		exit(1);
	}
	int connected = connect(sockfd, serveraddr->ai_addr,serveraddr->ai_addrlen);
	if(connected < 0)
	{
		perror("Cannot connect to the server\n");
		exit(3);
	}
	else {
		printf("Connected to the server %s at path %s\n", servername, path);
	}
	freeaddrinfo(serveraddr);
	char buffer[BUFFERSIZE]; 
	bzero(buffer,BUFFERSIZE); 
	sprintf(buffer, "GET /%s HTTP/1.0\r\nHost: %s\r\n\r\n",path, servername);
	int byte_sent = send(sockfd, buffer, strlen(buffer), 0);
	bzero(buffer,BUFFERSIZE);
	int byte_received = recv(sockfd, buffer, BUFFERSIZE, 0);
	if(byte_received <0)
	{
		perror("Error in reading\n");
		exit(4);
	}

	int responseCode;
	sscanf(buffer, "HTTP/1.%*[01] %d ", &responseCode);
	printf("Response code %d\n", responseCode);
	if (responseCode != 200){
		printf("Error occured while receiving the data from the server %d\n", responseCode);
		exit(5);
	}

	char *data = strstr(buffer, "\r\n\r\n");
	data = data + 4;
	char *filename = strrchr(URL, '/');
	filename = filename + 1 ;
	char *fileExists = strrchr(filename, '.');
	if(!fileExists) {
		filename = "index.html";
	}
	FILE *fp;
	fp = fopen(filename, "w");
	fwrite(data, byte_received, 1, fp);
	bzero(buffer, BUFFERSIZE);
	while ((byte_received = recv(sockfd, buffer, BUFFERSIZE, 0)) > 0) {
		fwrite(buffer, byte_received, 1, fp);
	}
	printf("Data written to the file %s\n", filename);
	fclose(fp);
	close(sockfd);
	return 0;
}
