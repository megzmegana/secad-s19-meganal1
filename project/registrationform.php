<html>
      <h1>Sign Up for new Account</h1>
<?php
  
  echo "Current time: " . date("Y-m-d h:i:sa")
?>
          <form action="addnewuser.php" method="POST" class="form login">
          		<br>
                Username:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="text" class="text_field" name="username" required
                pattern="^[\w.-]+@[\w-]+(.[\w-]+)*$"
                title="Please enter a valid email as username"
                placeholder="Your email address"
                onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: ");"/> <br><br>

                Password:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="password" class="text_field" name="password" required
                pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&])[\w!@#$%^&]{8,}$"
                placeholder="Your password"
                title="Password must has at least 8 characters with 1 special symbol !@#$%^& 1 number, 1
                lowercase, and 1 UPPERCASE"
                onchange="this.setCustomValidity(this.validity.patternMismatch?this.title:
                ''); form.repassword.pattern = this.value;"/> <br><br>

                Retype Password:&nbsp; 
                <input type="password" class="text_field" name="retypepassword"
                placeholder="Retype your password" required
                title="Password does not match"
                onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');"/> <br><br>

                Email: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="text" class="text_field" name="emailid" required
                pattern="^[\w.-]+@[\w-]+(.[\w-]+)*$"
                title="Please enter a valid email as username"
                placeholder="Your email address"
                onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: ");"/> <br><br>

                Phone:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="text" class="text_field" name="phone" required
                pattern="^([0-9]{10})"
                placeholder="Please enter phone number"
                onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');"/> <br><br>

                Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="text" class="text_field" name="name of the user" 
                placeholder="Please enter name"/> <br>
                <button class="button" type="submit">
                  Sign Up
                </button>
          </form>
</html>