<?php
    require "session_auth.php";
    require "database.php";
    $nocsrftoken = $_REQUEST["nocsrftoken"];
    if (!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
      echo "<script>alert('Cross-site request forgery is detected!');</script>";
      header("Refresh:0;url=logout.php");
      die();
    }
    $username = $_SESSION["username"]; //$_REQUEST["username"];
    $newpassword = $_REQUEST["newpassword"];
    if (isset($username) AND isset($newpassword)) {
      echo "DEBUG:changepassword.php->Got: username=$username;newpassword=$newpassword";
      if(changepassword($username,$newpassword)){
        echo "The new password has been set!";
      }else{
        echo "Change password failed!";
      }

    }else{
      echo "No provided username/password to change";
      exit();
    }
?>
