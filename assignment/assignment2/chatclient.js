var net = require('net');

if(process.argv.length != 4){
	console.log("Usage: node %s <host> <port>", process.argv[1]);
	process.exit(0);
}
var host=process.argv[2];
var port=process.argv[3];
var authenticated = false;
if(host.length >253 || port.length >5 ){
	console.log("Invalid host or port. Try again!\nUsage: node %s <port>", process.argv[1]);
	process.exit(1);
}
var client = new net.Socket();
console.log("Simple telnet.js developed by megana, SecAD-S19");
console.log("Connecting to: %s:%s", host, port);
client.connect(port,host, connected);
function connected(){
	console.log("Connected to: %s:%s", client.remoteAddress, client.remotePort);
	console.log("You need to login before sending/receiving message.\n");
	loginsync();
}
var readlineSync = require('readline-sync');
var username;
var password;
function loginsync(){
	username = readlineSync.question('username:');
	password = readlineSync.question('password:', {
	hideEchoBack: true
	});
	var creds = '{"username":"'+ username +'","password":"'+ password +'"}';
	client.write(creds);
}
client.on("data", (data)=> {
	console.log("Received data:" +data);
	chat();
});
client.on("error", function(err){
	console.log("Error");
	process.exit(2);
});
client.on("close", function(data){
	console.log("Connection has been disconnected");
	process.exit(3);
});
function chat(){
	const keyboard = require('readline').createInterface({
		input: process.stdin,
		output: process.stdout
	});
	keyboard.on("line", (input) => {
		if(input == "quit") {
			client.destroy();
			console.log("Connection Closed!");
			process.exit();
		}
		if (input == "help") {
			console.log("Choose your option");
			console.log("1 Message to  be sent as private message");
			console.log("2 Message to be sent as public message");
			console.log("3 LIst of all the online users");
		} else if (input == "1") {
			client.write("1");
		} else if (input == "2") {
			client.write("2");
		} else if (input == "3") {
			client.write("3");
		}
		else {
			client.write(input);
		}
	});
}