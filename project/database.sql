DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`(
	username varchar(50) NOT NULL,
	password varchar(100) NOT NULL,
	enabled int NOT NULL,
	super int NOT NULL,
	PRIMARY KEY(`username`)
);

DROP TABLE IF EXISTS `post`;
CREATE TABLE `post`(
	postid int NOT NULL AUTO_INCREMENT,
	username varchar(50) NOT NULL,
	info varchar(100),
	likes int,
	postdate varchar(50),
	PRIMARY KEY(`postid`),
	FOREIGN KEY(`username`) REFERENCES users(`username`)
);

INSERT INTO `users` VALUES ('megana', password('megana'), 1, 1);

