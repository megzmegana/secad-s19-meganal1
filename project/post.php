<?php
session_start();
require 'database.php';
if (isset($_POST['newpost'])) {
	$username = $_SESSION["username"];
	$info = $_POST['newpost'];
	if (addnewpost($username, $info)) {
		echo "Post added successfully";
	} else {
		echo "Failed to add post";
	}
}

?>
<h4>Posts</h4>
<form action="post.php" method="POST" class="form post">
	<textarea name="newpost" required cols="100" rows="10" title="Enter your post"></textarea><br><br>
	<button class="button" type="submit">
	Add Post
	</button>
	<br><br>
</form>
