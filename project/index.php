<?php
	$lifetime = 15 * 60; //15minutes  ---- limit the active session lifetime
	$path = "/miniBook";
	$domain = "secad-s19-meganal1.com";
	$secure = TRUE;//prevent man in the middle attacks
	$httponly = TRUE; // cannot be stolen by JavaScript(XSS)
	session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
	session_start();    
	if (isset($_POST["username"]) AND isset($_POST["password"])) {
		if(securechecklogin($_POST["username"],$_POST["password"])){
			$_SESSION["logged"] = TRUE;
			$_SESSION["username"] = $_POST["username"];
			$_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
		}
		else {
			echo "<script>alert('Invalid username/password');</script>";
			unset($_SESSION["logged"]);
			header("Refresh:0; url=form.php");
			die();
		}
	}
	if(!isset($_SESSION["logged"]) or $_SESSION["logged"] != TRUE){
		echo "<script>alert('You have not login. PLease login first');</script>";
		header("Refresh:0; url=form.php");
		die();
	}
	if($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]){
		echo "<script>alert('Session hijacking is detected!');</script>"; 
		header("Refresh:0; url=form.php");
		die();
	}

?>
	<h2> Welcome <?php echo htmlentities($_SESSION["username"]); ?> !</h2>
	<a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a>
	<br><br>


	<a href="post.php">Post</a><br><br>


	<form action="viewpost.php" method="POST" class="view post">
		<button class="button" type="submit">View all Posts</button>
	</form>

<?php	

	if (isAdmin($_SESSION["username"])) {
		echo "<a href='admin.php'>Handle Users</a>";
	}

	

  	function securechecklogin($username, $password){
  		$mysqli = new mysqli('localhost','meganal3','megana13','secad_s19_meganal1');
		if($mysqli->connect_errno){
			printf("Database connection failed: %s\n", $mysqli->connect_errno);
			exit();
		}
		$enabled = 1;
  		$prepared_sql = "SELECT * FROM users WHERE username= ? " ." AND password=password(?) AND enabled=?";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			echo "Prepared Statement Error";
		$stmt->bind_param("ssi", $username,$password, $enabled);
		if(!$stmt->execute()) echo "Execute ERROR";
		if(!$stmt->store_result()) echo "Store_result ERROR";
		$result = $stmt;
		if($result->num_rows == 1)
			return TRUE;
		return FALSE;
  	}

?>
