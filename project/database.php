<?php
	$mysqli = new mysqli('localhost','meganal3','megana13','secad_s19_meganal1');
			if($mysqli->connect_errno){
				printf("Database connection failed: %s\n", $mysqli->connect_errno);
				exit();
	}
	function changepassword($username, $newpassword){
	  		global $mysqli;
	  		$prepared_sql = "UPDATE users SET password= password(?) WHERE username= ?; ";
	  		echo "DEBUG:database->prepared_sql=$prepared_sql<br>";

			if(!$stmt = $mysqli->prepare($prepared_sql))
				return FALSE;
			$stmt->bind_param("ss", $newpassword, $username);
			if(!$stmt->execute()) return FALSE;
			return TRUE;
	}
  	function addnewuser($username, $password,$phone,$emailid, $enabled, $super){
  		global $mysqli;
  		$prepared_sql = "INSERT INTO users (username, password, phone, emailid, enabled, super) VALUES (?, password(?), ?, ?, ?, ?);";
		if(!$stmt = $mysqli->prepare($prepared_sql)) 
			return FALSE;
		$stmt->bind_param("ssisii", $username, $password, $phone, $emailid, $enabled, $super);
		if(!$stmt->execute()) 
			return FALSE;
		return TRUE;
  	}
  	function addnewpost($username, $info){
  		global $mysqli;
  		$prepared_sql = "INSERT INTO post (username, info) VALUES (?, ?);";
		if(!$stmt = $mysqli->prepare($prepared_sql)) 
			return FALSE;
		$stmt->bind_param("ss", $username,$info);
		if(!$stmt->execute()) 
			return FALSE;
		return TRUE;
  	}
  	
  	function view_singlepost($postid){
  		global $mysqli;
  		$sql="SELECT info FROM post WHERE postid= ?;";
  		if(!$stmt = $mysqli->prepare($sql))
  			return "Error";
  		$stmt->bind_param("i", $postid);
  		if(!$stmt->execute())
  			return "Execute error";
  		$info=NULL;
  		if(!$stmt->bind_result($info))
  			return "Binding failed";
  		if($stmt->fetch()){
  			//echo htmlentities($postid) . ", " . htmlentities($info) . "<br>";
  			return htmlentities($info);
  		} else {
  			return "";
  		}
  	}


  	function show_allposts(){
  		global $mysqli;
  		$sql = "SELECT postid,info FROM post";
  		$result = $mysqli->query($sql);
  		if($result->num_rows>0){
  			echo "Total number of posts = " . $result->num_rows;
  			echo "<br>";

  			while($rows=$result->fetch_assoc()){
  				echo"<br>";
  				$postid = $rows['postid'];
  				echo "postid=" . $rows["postid"] . " " . "; 
  				post is = " .$rows["info"] . "</br><a href='editpost.php?postid=".$rows["postid"] ."'> Edit</a> | ";
  				echo "<a href='comment.php?postid=$postid'>Comment</a>";
  				echo "</br>";  				

  			}
  		}
  	}

  	function show_allusers(){
  		global $mysqli;
  		$sql = "SELECT * FROM users where super=0";
  		$result = $mysqli->query($sql);

  		if($result->num_rows>0){
  			echo "Total number of users = " . $result->num_rows;
  			echo "<form action='admin.php' method='POST'>";
  			echo "<table>";

  			while($rows=$result->fetch_assoc()) {
  				echo"<tr>";
  				$username = $rows['username'];
  				$checked="";
  				if ($rows["enabled"] == 1) {
  					$checked = " checked ";
  				}
  				$user = $rows['username'];
  				echo "<td>" . $rows["username"] . " </td>";
  				echo "<td><input type='hidden' name='adminchanges'></input></td>";
  				echo "<td><input type='checkbox' $checked name=".$user."></input></td>";
  				echo "</tr>";
  			}
  			echo "</table><button type='submit'>Save</button>";
  			echo "</form>";
  		}
  	}

  	function changepost($postid, $info){
  		echo "its edit post";
  		global $mysqli;
  		$sql="UPDATE post SET info=? where postid=?;";
  		if(!$stmt=$mysqli->prepare($sql))
  			echo "error";
  		echo $sql;
  		$stmt->bind_param("ss", htmlspecialchars($info), htmlspecialchars($postid));
  		if(!$stmt->execute()){
  			echo "error";
  			return FALSE;
  		}
  		return TRUE;
  	}


  	function addComment($postid, $comments, $commentedby){
			echo "comment";
			global $mysqli;
			$sql="INSERT INTO comment (postid, comments, commentedby) VALUES(?,?,?);";
			if (!$stmt=$mysqli->prepare($sql)) {
				echo "errorasd";
				return FALSE;
			}
			echo $sql;
			$stmt->bind_param("iss", $postid, $comments, $commentedby);
			if(!$stmt->execute()){
  			echo "error123";
  			return FALSE;
			}
			return TRUE;
			
  	}

  	function showAllComments($postid) {
  		global $mysqli;
  		$sql = "SELECT comments, commentedby FROM comment where postid=?";
  		if (!$stmt=$mysqli->prepare($sql)) {
				echo "errorasd";
				return FALSE;
			}
			$stmt->bind_param("i", $postid);
			if (!$stmt->execute()) {
				return FALSE;
			}
			$results = $stmt->get_result();
			while($rows = mysqli_fetch_assoc($results)) {
				echo"<br>";
				echo $rows['commentedby']." said ".$rows['comments'];
			}
  	}

  	function enableUser($username, $enabled) {
  		global $mysqli;
  		$prepared_sql="UPDATE users SET enabled=? where username=?;";
  		if(!$stmt=$mysqli->prepare($prepared_sql)) {
  			echo "error";
  			return FALSE;
  		}
  		echo "DEBUG>enableUser->SQL=" . $sql;
  		echo "enabled=" .$enabled;
  		echo "username=" .$username;
  		$stmt->bind_param("is", htmlspecialchars($enabled), htmlspecialchars($username));
  		if(!$stmt->execute()){
  			echo "error123";
  			return FALSE;
  		}
  		return TRUE;
  	}

  	function isAdmin($username) {
		global $mysqli;
		if($mysqli->connect_errno){
			printf("Database connection failed: %s\n", $mysqli->connect_errno);
			exit();
		}
		$super = 1;
  		$prepared_sql = "SELECT * FROM users WHERE username= ?";
		if(!$stmt = $mysqli->prepare($prepared_sql)) {
			echo "Prepared Statement Error";
			return FALSE;
		}
		$stmt->bind_param("si", $username, $super);
		if(!$stmt->execute()) {
			echo "Execute ERROR";
			return FALSE;
		}
		if(!$stmt->store_result()) {
			echo "Store_result ERROR";
			return FALSE;
		}
		$result = $stmt;
		if($result->num_rows ==1)
			return TRUE;
		return FALSE;
  	}



  	function mysql_checklogin_secure_admin($username, $password) { 
	    global $mysqli;
	    $prepared_sql = "SELECT * FROM admin where username= ?". " and password=password(?);";
	    if(!$stmt = $mysqli->prepare($prepared_sql))
		   echo "Prepared Statement Error";
		   $stmt->bind_param("ss", htmlspecialchars($username),htmlspecialchars($password));
		  if(!$stmt->execute()) 
		   echo "Execute Error";
		  if(!$stmt->store_result()) 
		   echo "Store_result Error";
		  if ($stmt->num_rows == 1) return TRUE;
		   return FALSE;

  }


  	function mysql_change_users_password_admin($username, $newpassword) { 
	    global $mysqli;
	    $prepared_sql = "UPDATE admin SET password=password(?) WHERE username=?;";
  
		  if(!$stmt = $mysqli->prepare($prepared_sql))
		   echo "Prepared Statement Error";
		   $stmt->bind_param("ss",$newpassword, $username);
		  if(!$stmt->execute()) {
		   echo "Execute Error: UPDATE users SET password=password(?) WHERE username=?;";
		  return FALSE;
		  } 
		   return TRUE;
  }


?>