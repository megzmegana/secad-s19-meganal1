package main

import (

	"fmt"

	"net"
	"encoding/json"
	"os"
	"io/ioutil"
	"crypto/sha256"
	"encoding/hex"
	"golang.org/x/crypto/pbkdf2"

)
const BUFFERSIZE int = 1024
var buffer [BUFFERSIZE]byte
var allClient_conns = make(map[net.Conn]string)
var newclient = make(chan net.Conn)
var userDB UserDB

type UserDB struct{
	Users []Account `json:"users"`
}
type Account struct{
	Username string `json="username"`
	Password string `json="username"`
}

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s <port>\n", os.Args[0])
		os.Exit(0)
	}
	port := os.Args[1]
	if len(port) > 5 {
		fmt.Println("Invalid port value. Try again!")
		os.Exit(1)
	}
	server, err := net.Listen("tcp", ":"+port)
	if err != nil {
		fmt.Printf("Cannot listen on port '" + port + "'!\n")
		os.Exit(2)
	}

	fmt.Println("EchoServer in GoLang customized by Megana, SecAD-S19")

	fmt.Printf("EchoServer is listening on port '%s' ...\n", port)

	go func() {
	  	for {
		    client_conn, _ := server.Accept()
		    loginData(client_conn)
		    newclient <- client_conn
	    }
	}()
    for {
        select {
            case client_conn :=<-newclient:
                go client_goroutine(client_conn, allClient_conns)
        }
    }
}
func authenticationUser(username string, password string, client_conn net.Conn) bool {
	fmt.Println("Username=" +username+",password="+password)
	if CheckAccount(username,password){
		allClient_conns[client_conn] = username
		return true
	}
	return false
}

func loginData(client_conn net.Conn) {
	byte_received, read_err := client_conn.Read(buffer[0:])
	if read_err != nil{
		fmt.Printf("Error in reading")
		return
	}
	data := buffer[0:byte_received]
	fmt.Println("Data received = %s", data)
	var account Account
	err :=json.Unmarshal(data, &account)
	if (err != nil) {
		sendToClient(client_conn, [] byte("Login first to use the chat server .\nUsage: {\"username\":\"<your username>\",\"password\":\"<your password>\" and press Enter/n"))
		panic(err)
		//sendToClient(client_conn, []byte("LOGIN IS FAILED. PLEASE TRY AGAIN"),client_conn)
	}
	fmt.Println(account.Username + ", " + account.Password)
	if authenticationUser(account.Username, account.Password, client_conn) {
		fmt.Printf("Ok. Success")
		sendToClient(client_conn, []byte("You are Authenticated\n"))
	} else {
		fmt.Printf("fail")
		sendToClient(client_conn, []byte("Authentication failed. Try again\n"))
		loginData(client_conn)
	}
}

func client_goroutine(client_conn net.Conn, allClient_conns map[net.Conn]string) {
    fmt.Printf("# of connected clients: %d\n",len(allClient_conns))
	for {
		byte_received, read_err := client_conn.Read(buffer[0:])
		if read_err != nil {
			fmt.Println("Error in receiving...")
			fmt.Println("Disconnected Client")
			return
		}
                _, write_err := client_conn.Write(buffer[0:byte_received])
		if write_err != nil {
			fmt.Println("Error in receiving...")
			return
		}
		data := buffer[0:byte_received]
        go sendtoAll(allClient_conns, data)
		fmt.Printf("Received data: %sEchoed back!\n", buffer)
	   }
}

func sendtoAll(allClient_conns map[net.Conn]string, data []byte){
    for client_conn, _ := range allClient_conns{
        _, write_err := client_conn.Write(data)
		if write_err != nil {
			fmt.Println("Error in receiving...")
			return
                }
    }
}

func sendToClient(client_conn net.Conn, data []byte){
    _, write_err := client_conn.Write(data)
	if write_err != nil {
		fmt.Println("Error in receiving...")
		return
            }
}

func OpenUsersDB() {
	filepointer, open_err := os.Open("users-database.json")
	defer filepointer.Close()
	if open_err != nil {
		fmt.Println("No users-database.json file. Need to add a new user to create one.")
		json.Unmarshal([]byte(`{"users":[]}`), & userDB)	
	} else {
		jsondatabase, read_err := ioutil.ReadAll(filepointer)
		if read_err != nil {
			fmt.Println("Error in reading the JSON data")
			os.Exit(1)
		}
		unmarshal_err := json.Unmarshal(jsondatabase, & userDB)
		if unmarshal_err != nil {
			fmt.Println("Error in mapping the JSON data")
			os.Exit(1)
		}
	}	
	fmt.Println("User data loaded")
}

func getHash(salt string, data string) string {
	return hex.EncodeToString(pbkdf2.Key([]byte(data),[]byte(salt), 4096, 32, sha256.New))
}


func  CheckAccount(username string, password string) bool{
    OpenUsersDB()
    for i := 0; i < len(userDB.Users); i++ {
		if username == userDB.Users[i].Username && getHash(username, password) == userDB.Users[i].Password {
			return true
		}
	}
	return false
}



/*func jsonmanipulation(){
	filepointer, _ := os.Open("filename")
	jsondatabase, _ := ioutil.ReadAll(filepointer)
	json.Unmarshal(jsondatabase, &userDB)
	OpenUsersDB()
}*/