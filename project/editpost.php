<?php
	require 'database.php';
	session_start();
	$postid = $_REQUEST['postid'];
	$updatedinfo = view_singlepost($postid);
?>

<html>
      <h1>Edit Post</h1>
      <a href ="index.php"><h3>Home</h3></a>
<?php

	if (empty($updatedinfo)) {
		echo "No Records Found";
	}
	echo "<br>";

?>
	<form action="changepost.php" method="POST" class="form login">
		<input type="hidden" name="postid" value="<?php echo $postid ?>">
	    <textarea name="updatedinfo" required cols="90" rows="9" ><?php echo $updatedinfo; ?></textarea>
	    <br><br>
	    <button class="button" type="submit">
	      Change post
	    </button>
	</form>
</html>

